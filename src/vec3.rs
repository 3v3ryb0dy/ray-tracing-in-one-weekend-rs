use crate::rtweekend::{random_float, random_float_min_max};
use std::{
    fmt::Display,
    ops::{Add, AddAssign, Div, DivAssign, Index, Mul, MulAssign, Neg, Sub},
};

pub type Point3 = Vec3;
pub type Color = Vec3;

#[derive(Copy, Clone)]
struct Coord3D(f32, f32, f32);

#[derive(Copy, Clone)]
pub struct Vec3 {
    e: Coord3D,
}

impl Vec3 {
    pub fn new(e0: f32, e1: f32, e2: f32) -> Vec3 {
        Vec3 {
            e: Coord3D(e0, e1, e2),
        }
    }

    pub fn random() -> Vec3 {
        Vec3 {
            e: Coord3D(random_float(), random_float(), random_float()),
        }
    }

    pub fn random_min_max(min: f32, max: f32) -> Vec3 {
        Vec3 {
            e: Coord3D(
                random_float_min_max(min, max),
                random_float_min_max(min, max),
                random_float_min_max(min, max),
            ),
        }
    }

    pub fn near_zero(&self) -> bool {
        let s = 1e-8f32;
        (self.x().abs() < s) && (self.y().abs() < s) && (self.y().abs() < s)
    }

    pub fn x(&self) -> f32 {
        self.e.0
    }

    pub fn y(&self) -> f32 {
        self.e.1
    }

    pub fn z(&self) -> f32 {
        self.e.2
    }

    pub fn length(&self) -> f32 {
        f32::sqrt(self.length_squared())
    }

    pub fn length_squared(&self) -> f32 {
        self.e.0 * self.e.0 + self.e.1 * self.e.1 + self.e.2 * self.e.2
    }
}

impl Default for Vec3 {
    fn default() -> Self {
        Self {
            e: Coord3D(0.0, 0.0, 0.0),
        }
    }
}

impl Neg for Vec3 {
    type Output = Self;

    fn neg(mut self) -> Self::Output {
        self.e.0 = -self.e.0;
        self.e.1 = -self.e.1;
        self.e.2 = -self.e.2;
        self
    }
}

impl Index<usize> for Vec3 {
    type Output = f32;

    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.e.0,
            1 => &self.e.1,
            2 => &self.e.2,
            _ => panic!("Index out of bounds"),
        }
    }
}

impl AddAssign for Vec3 {
    fn add_assign(&mut self, rhs: Self) {
        self.e.0 += rhs.e.0;
        self.e.1 += rhs.e.1;
        self.e.2 += rhs.e.2;
    }
}

impl MulAssign<f32> for Vec3 {
    fn mul_assign(&mut self, rhs: f32) {
        self.e.0 *= rhs;
        self.e.1 *= rhs;
        self.e.2 *= rhs;
    }
}

impl DivAssign<f32> for Vec3 {
    fn div_assign(&mut self, rhs: f32) {
        self.e.0 /= rhs;
        self.e.1 /= rhs;
        self.e.2 /= rhs;
    }
}

impl Display for Vec3 {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{} {} {}", self.e.0, self.e.1, self.e.2)
    }
}

impl Add for Vec3 {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Vec3 {
            e: Coord3D(self.e.0 + rhs.e.0, self.e.1 + rhs.e.1, self.e.2 + rhs.e.2),
        }
    }
}

impl Sub for Vec3 {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Vec3 {
            e: Coord3D(self.e.0 - rhs.e.0, self.e.1 - rhs.e.1, self.e.2 - rhs.e.2),
        }
    }
}

impl Mul for Vec3 {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        Vec3 {
            e: Coord3D(self.e.0 * rhs.e.0, self.e.1 * rhs.e.1, self.e.2 * rhs.e.2),
        }
    }
}

impl Mul<f32> for Vec3 {
    type Output = Self;

    fn mul(self, rhs: f32) -> Self::Output {
        Vec3 {
            e: Coord3D(self.e.0 * rhs, self.e.1 * rhs, self.e.2 * rhs),
        }
    }
}

impl Mul<Vec3> for f32 {
    type Output = Vec3;

    fn mul(self, rhs: Vec3) -> Self::Output {
        rhs * self
    }
}

impl Div<f32> for Vec3 {
    type Output = Self;

    fn div(self, rhs: f32) -> Self::Output {
        (1.0 / rhs) * self
    }
}

pub fn dot(lhs: &Vec3, rhs: &Vec3) -> f32 {
    lhs.e.0 * rhs.e.0 + lhs.e.1 * rhs.e.1 + lhs.e.2 * rhs.e.2
}

pub fn cross(lhs: &Vec3, rhs: &Vec3) -> Vec3 {
    Vec3::new(
        lhs.e.1 * rhs.e.2 - lhs.e.2 * rhs.e.1,
        lhs.e.2 * rhs.e.0 - lhs.e.0 * rhs.e.2,
        lhs.e.0 * rhs.e.1 - lhs.e.1 * rhs.e.0,
    )
}

pub fn unit_vector(v: Vec3) -> Vec3 {
    let length = v.length();
    v / length
}

pub fn random_in_unit_sphere() -> Vec3 {
    loop {
        let p = Vec3::random_min_max(-1.0, 1.0);
        if p.length_squared() >= 1.0 {
            continue;
        }
        return p;
    }
}

pub fn random_unit_vector() -> Vec3 {
    unit_vector(random_in_unit_sphere())
}

pub fn random_in_hemisphere(normal: &Vec3) -> Vec3 {
    let in_unit_sphere = random_in_unit_sphere();
    if dot(&in_unit_sphere, normal) > 0.0 {
        in_unit_sphere
    } else {
        -in_unit_sphere
    }
}

pub fn random_in_unit_disk() -> Vec3 {
    loop {
        let p = Vec3::new(
            random_float_min_max(-1.0, 1.0),
            random_float_min_max(-1.0, 1.0),
            0.0,
        );
        if p.length_squared() >= 1.0 {
            continue;
        }
        return p;
    }
}

pub fn reflect(v: &Vec3, n: &Vec3) -> Vec3 {
    *v - 2f32 * dot(v, n) * *n
}

pub fn refract(uv: &Vec3, n: &Vec3, etai_over_etat: f32) -> Vec3 {
    let cos_theta = f32::min(dot(&-*uv, n), 1.0);
    let r_out_perp = etai_over_etat * (*uv + cos_theta * *n);
    let r_out_parallel = -f32::sqrt(f32::abs(1.0 - r_out_perp.length_squared())) * *n;
    r_out_perp + r_out_parallel
}
