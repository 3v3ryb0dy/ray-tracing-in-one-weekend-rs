pub const INFINITY: f32 = f32::INFINITY;
pub const PI: f32 = std::f32::consts::PI;

pub fn degrees_to_radian(degrees: f32) -> f32 {
    degrees * PI / 180.0
}

pub fn random_float() -> f32 {
    rand::random::<f32>()
}

pub fn random_float_min_max(min: f32, max: f32) -> f32 {
    (min + (max - min)) * random_float()
}

pub fn clamp(value: f32, min: f32, max: f32) -> f32 {
    value.clamp(min, max)
}
