#![allow(dead_code, unused_imports, unused_variables)]
mod camera;
mod color;
mod hittable;
mod material;
mod ray;
mod rtweekend;
mod sphere;
mod vec3;
use camera::Camera;
use color::print_color;
use hittable::{HitRecord, Hittable, HittableList};
use material::Material;
use ray::Ray;
use rayon::prelude::*;
use rtweekend::{random_float_min_max, INFINITY};
use sphere::Sphere;
use std::{
    rc::Rc,
    sync::{Arc, Mutex},
    thread,
};
use vec3::{unit_vector, Color, Point3};

use crate::{
    material::{Dielectric, Lambertian, Metal},
    rtweekend::random_float,
    vec3::Vec3,
};

fn ray_color<T: Hittable>(ray: &mut Ray, hittable: &T, depth: usize) -> Color {
    if depth == 0 {
        return Color::default();
    }

    let mut rec = HitRecord::default();
    if hittable.hit(ray, 0.001, INFINITY, &mut rec) {
        let mut scattered = Ray::default();
        let mut attenuation = Color::default();

        if rec
            .material
            .clone()
            .unwrap()
            .scatter(ray, &mut rec, &mut attenuation, &mut scattered)
        {
            return attenuation * ray_color(&mut scattered, hittable, depth - 1);
        }

        return Color::default();
    }

    let unit_direction = unit_vector(ray.direction());
    let t = 0.5 * (unit_direction.y() + 1.0);
    (1.0 - t) * Color::new(1.0, 1.0, 1.0) + t * Color::new(0.5, 0.7, 1.0)
}

fn random_scene() -> HittableList<Sphere> {
    let mut world = HittableList::default();

    let material_ground = Lambertian::new(Color::new(0.5, 0.5, 0.5));

    let material_ground = Box::new(material_ground);

    world.add(Sphere::new(
        &Point3::new(0.0, -1000.0, 0.0),
        1000.0,
        material_ground,
    ));

    for a in -11..11 {
        for b in -11..11 {
            let choose_mat = random_float();
            let center = Point3::new(
                a as f32 + 0.9 * random_float(),
                0.2,
                b as f32 + 0.9 * random_float(),
            );

            if (center - Point3::new(4.0, 0.2, 0.0)).length() > 0.9 {
                let sphere_material: Box<dyn Material + Send + Sync>;
                if choose_mat < 0.8 {
                    // diffuse
                    let albedo = Color::random() * Color::random();
                    sphere_material = Box::new(Lambertian::new(albedo));
                    world.add(Sphere::new(&center, 0.2, sphere_material));
                } else if choose_mat < 0.95 {
                    // metal
                    let albedo = Color::random_min_max(0.5, 1.0);
                    let fuzz = random_float_min_max(0.0, 0.5);
                    sphere_material = Box::new(Metal::new(albedo, fuzz));
                    world.add(Sphere::new(&center, 0.2, sphere_material));
                } else {
                    sphere_material = Box::new(Dielectric::new(1.5));
                    world.add(Sphere::new(&center, 0.2, sphere_material));
                }
            }
        }
    }

    let material1 = Box::new(Dielectric::new(1.5));
    world.add(Sphere::new(&Point3::new(0.0, 1.0, 0.0), 1.0, material1));

    let material2 = Box::new(Lambertian::new(Color::new(0.4, 0.2, 0.1)));
    world.add(Sphere::new(&Point3::new(-4.0, 1.0, 0.0), 1.0, material2));

    let material3 = Box::new(Metal::new(Color::new(0.4, 0.2, 0.1), 0.0));
    world.add(Sphere::new(&Point3::new(4.0, 1.0, 0.0), 1.0, material3));
    world
}

fn main() {
    // Image
    const ASPECT_RATIO: f32 = 16.0 / 9.0;
    const IMAGE_WIDTH: i32 = 500;
    const IMAGE_HEIGHT: i32 = (IMAGE_WIDTH as f32 / ASPECT_RATIO) as i32;
    const SAMPLES_PER_PIXEL: usize = 500usize;
    const MAX_DEPTH: usize = 50usize;

    // World

    let world = random_scene();

    let look_from = Point3::new(13.0, 2.0, 3.0);
    let look_at = Point3::new(0.0, 0.0, 0.0);
    let v_up = Vec3::new(0.0, 1.0, 0.0);
    let focus_dist = 10.0;
    let aperture = 0.1;

    // Camera
    let camera = Arc::new(Camera::new(
        look_from,
        look_at,
        v_up,
        20.0,
        ASPECT_RATIO,
        aperture,
        focus_dist,
    ));

    // Render

    println!("P3");
    println!("{} {}", IMAGE_WIDTH, IMAGE_HEIGHT);
    println!("255");

    let pixels = Arc::new(Mutex::new(
        [[Color::default(); IMAGE_WIDTH as usize]; IMAGE_HEIGHT as usize],
    ));

    let remaining = Arc::new(Mutex::new(IMAGE_HEIGHT));

    (0..(IMAGE_HEIGHT)).into_par_iter().for_each(|j| {
        let mut remaining = remaining.lock().unwrap();
        *remaining -= 1;
        eprintln!("\rScanlines remaining: {}", remaining);
        drop(remaining);

        let world = world.clone();
        let camera = camera.clone();

        for i in 0..IMAGE_WIDTH {
            let mut pixel_color = Color::default();

            for _ in 0..SAMPLES_PER_PIXEL {
                let u = (i as f32 + random_float()) / (IMAGE_WIDTH as f32 - 1.0);
                let v = (j as f32 + random_float()) / (IMAGE_HEIGHT as f32 - 1.0);

                let mut ray = camera.get_ray(u, v);
                pixel_color += ray_color(&mut ray, &world, MAX_DEPTH);
            }
            pixels.lock().unwrap()[j as usize][i as usize] = pixel_color;
        }
    });

    for (i, line) in pixels.lock().unwrap().into_iter().enumerate().rev() {
        for pixel in line {
            print_color(pixel, SAMPLES_PER_PIXEL);
        }
    }
}
