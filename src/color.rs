use crate::{rtweekend::clamp, vec3::Color};

pub fn print_color(color: Color, samples_per_pixel: usize) {
    let mut r = color[0];
    let mut g = color[1];
    let mut b = color[2];

    let scale = 1.0 / samples_per_pixel as f32;
    r = (scale * r).sqrt();
    g = (scale * g).sqrt();
    b = (scale * b).sqrt();

    let r = (256f32 * clamp(r, 0.0, 0.999)) as u8;
    let g = (256f32 * clamp(g, 0.0, 0.999)) as u8;
    let b = (256f32 * clamp(b, 0.0, 0.999)) as u8;

    println!("{} {} {}", r, g, b);
}
