use std::{
    borrow::Borrow,
    rc::Rc,
    sync::{Arc, Mutex},
};

use crate::{
    material::Material,
    ray::Ray,
    vec3::{dot, Point3, Vec3},
};

#[derive(Clone, Default)]
pub struct HitRecord {
    pub p: Point3,
    normal: Vec3,
    pub t: f32,
    pub front_face: bool,
    pub material: Option<Arc<Box<dyn Material + Send + Sync>>>,
}

impl HitRecord {
    pub fn get_face_normal(&mut self) -> Vec3 {
        self.normal
    }

    pub fn set_face_normal(&mut self, r: &Ray, outward_normal: &Vec3) {
        self.front_face = dot(&r.direction(), outward_normal) < 0.0;
        self.normal = if self.front_face {
            *outward_normal
        } else {
            -*outward_normal
        };
    }
}

pub trait Hittable {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32, rec: &mut HitRecord) -> bool;
}

#[derive(Clone)]
pub struct HittableList<T: Hittable> {
    hittables: Vec<T>,
}

impl<T: Hittable> Default for HittableList<T> {
    fn default() -> Self {
        Self {
            hittables: Default::default(),
        }
    }
}

impl<T: Hittable> HittableList<T> {
    pub fn new(hittable: T) -> Self {
        let hittables = vec![hittable];
        HittableList { hittables }
    }

    pub fn clear(&mut self) {
        self.hittables.clear();
    }

    pub fn add(&mut self, hittable: T) {
        self.hittables.push(hittable);
    }
}

impl<T: Hittable> Hittable for HittableList<T> {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32, rec: &mut HitRecord) -> bool {
        let mut hit_anything = false;
        let mut closest_so_far = t_max;

        for hittable in &self.hittables {
            let mut tmp_rec = HitRecord::default();
            if hittable.hit(ray, t_min, closest_so_far, &mut tmp_rec) {
                hit_anything = true;
                closest_so_far = tmp_rec.t;
                *rec = tmp_rec;
            }
        }

        hit_anything
    }
}
