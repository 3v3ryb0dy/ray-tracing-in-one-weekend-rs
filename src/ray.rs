use crate::vec3::{Point3, Vec3};

pub struct Ray {
    origin: Point3,
    direction: Vec3,
}

impl Default for Ray {
    fn default() -> Self {
        Self {
            origin: Point3::new(0.0, 0.0, 0.0),
            direction: Vec3::new(0.0, 0.0, 0.0),
        }
    }
}

impl Ray {
    pub fn new(origin: &Point3, direction: &Vec3) -> Ray {
        Ray {
            origin: Point3::new(origin.x(), origin.y(), origin.z()),
            direction: Vec3::new(direction.x(), direction.y(), direction.z()),
        }
    }

    pub fn origin(&self) -> Point3 {
        Point3::new(self.origin.x(), self.origin.y(), self.origin.z())
    }

    pub fn direction(&self) -> Vec3 {
        Vec3::new(self.direction.x(), self.direction.y(), self.direction.z())
    }

    pub fn at(&self, t: f32) -> Point3 {
        self.origin() + self.direction() * t
    }
}
