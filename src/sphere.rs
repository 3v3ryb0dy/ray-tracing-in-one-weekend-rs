use std::rc::Rc;
use std::sync::{Arc, Mutex};

use crate::hittable::{HitRecord, Hittable};
use crate::material::Material;
use crate::ray::Ray;
use crate::vec3::{dot, Point3};

#[derive(Clone)]
pub struct Sphere {
    center: Point3,
    radius: f32,
    material: Arc<Box<dyn Material + Send + Sync>>,
}

impl Sphere {
    pub fn new(center: &Point3, radius: f32, material: Box<dyn Material + Send + Sync>) -> Sphere {
        Sphere {
            center: Point3::new(center.x(), center.y(), center.z()),
            radius,
            material: Arc::new(material),
        }
    }
}

impl Hittable for Sphere {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32, rec: &mut HitRecord) -> bool {
        let oc = ray.origin() - self.center;
        let a = ray.direction().length_squared();
        let half_b = dot(&oc, &ray.direction());
        let c = oc.length_squared() - self.radius * self.radius;

        let discriminant = half_b * half_b - a * c;
        if discriminant < 0.0 {
            return false;
        }
        let sqrt = discriminant.sqrt();

        let mut root = (-half_b - sqrt) / a;

        if root < t_min || t_max < root {
            root = (-half_b + sqrt) / a;
            if root < t_min || t_max < root {
                return false;
            }
        }

        rec.t = root;
        rec.p = ray.at(rec.t);
        let outward_normal = (rec.p - self.center) / self.radius;
        rec.set_face_normal(ray, &outward_normal);
        rec.material = Some(Arc::clone(&self.material));

        true
    }
}
