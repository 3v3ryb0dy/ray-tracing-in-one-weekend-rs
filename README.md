# Ray Tracing in One Weekend (Rust)

This project follows the excellent book [Ray Tracing in One Weekend](https://raytracing.github.io/books/RayTracingInOneWeekend.html) by Peter Shirley and implements the Ray Tracer described there in the Rust programming language.

Since this is a CPU-only implementation, it is not intended for real-world use cases and only acts as a learning project.

> NOTE: This programm currently segfaults when trying to set an `IMAGE_WIDTH` greater than 600.

## Structure

This project follows the structure described in the book. For every significant addition of a feature, I tried to make a snapshot of the code and results, yielding the following stages:

### 2. Output an Image

![IMG1](img/1_img.jpeg)

### 4. Rays, a Simple Camera, and Background

![IMG2](img/2_gradient.jpeg)

### 5. Adding a Sphere

![IMG3](img/3_sphere.jpeg)

### 6. Surface Normals and Multiple Objects

![IMG4](img/4_normals.jpeg)
![IMG5](img/5_world.jpeg)

### 7. Antialiasing

![IMG6](img/6_antialiasing.jpeg)

### 8. Diffuse Materials

![IMG7_1](img/7_1_diffuse.jpeg)
![IMG7_2](img/7_2_gamma.jpeg)
![IMG7_3](img/7_3_shadow_acne.jpeg)
![IMG7_4](img/7_4_lambertian.jpeg)
![IMG7_5](img/7_5_hemispheric.jpeg)

### 9. Metal

![IMG8](img/8_materials.jpeg)
![IMG8_1](img/8_1_fuzziness.jpeg)

### 10. Dielectrics

![IMG9](img/9_dielectrics.jpeg)

### 11. Positionable Camera

![IMG10](img/10_camera.jpeg)

### 12. Defocus Blur

![IMG11](img/11_blur.jpeg)

### 13. Where Next?

![IMG12](img/12_final_scene.jpeg)
